pyth (0.6.0-3) UNRELEASED; urgency=medium

  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Mon, 14 May 2018 07:59:01 +0200

pyth (0.6.0-2) unstable; urgency=medium

  [ Daniele Tricoli ]
  * debian/compat
    - Bump compat level to 11.
  * debian/control
    - Bump debhelper version to >= 11.
    - Switch to python-bs4 in Build-Depends and Depends. (Closes: #891101)
    - Bump Standards-Version to 4.1.3 (no changes needed).
    - Add autodep8 tests.
    - Add X-Python-Version field.
  * debian/copyright
    - Update copyright years
  * debian/patches/switch-to-bs4.patch
    - Switch from python-beautifulsoup to python-bs4.
  * debian/watch
    - Use pypi.debian.net redirector.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

 -- Daniele Tricoli <eriol@mornie.org>  Wed, 28 Feb 2018 02:28:42 +0100

pyth (0.6.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Daniele Tricoli ]
  * New upstream release
  * Switch to pybuild
  * debian/compat
    - Bump debhelper compatibility level to 9
  * debian/control
    - Bump debhelper B-D to (>= 9)
    - Add dh-python to B-D
    - Bump Standards-Version to 3.9.5 (no changes needed)
  * debian/copyright
    - Update copyright years
    - Update Format URL to version 1.0
  * debian/patches/01_fix-refactored-PDFWriter-write.patch
    - Remove since applied upstream
  * debian/patches/02_fix-escaping-html-entities-behavior-change.patch
    - Remove since applied upstream
  * debian/rules
    - Use a custom pybuild system to invoke nose
  * debian/watch
    - Switch download URL to HTTPS
    - Remove debian and uupdate options

 -- Daniele Tricoli <eriol@mornie.org>  Wed, 23 Jul 2014 02:14:08 +0200

pyth (0.5.6-3) unstable; urgency=low

  * debian/control
    - Fixed Vcs-Svn URL
    - Sorted Build-Depends and Depends
    - Bumped Standards-Version to 3.9.3 (no changes needed)
  * debian/patches/01_fix-refactored-PDFWriter-write.patch
    - Made DEP3 compliant
  * debian/02_fix-escaping-html-entities-behavior-change.patch
    - Fixed escaping HTML entities within strings behavior change in
      python-beautifulsoup 3.2.1. (Closes: #678754)

 -- Daniele Tricoli <eriol@mornie.org>  Mon, 25 Jun 2012 05:25:43 +0200

pyth (0.5.6-2) unstable; urgency=low

  * debian/control
    - Fixed Vcs-Browser URL
    - Bumped Standards-Version to 3.9.2 (no changes needed)
    - Added packages needed for unit tests in Build-Depends field
    - Moved packages listed in Recommends field to Depends field
  * debian/copyright
    - Made DEP5 compliant
  * debian/{control, rules}
    - Switched to dh_python2
    - Run unit tests at build time using python-nose
  * debian/patches/01_fix-refactored-PDFWriter-write.patch
    - Fix refactored signature of PDFWriter.write in upstream git revision
      d87f7877cca5ea90c2d79979521a3db77c47eb30

 -- Daniele Tricoli <eriol@mornie.org>  Tue, 17 May 2011 06:20:48 +0200

pyth (0.5.6-1) unstable; urgency=low

  * Initial release (Closes: #590037)

 -- Daniele Tricoli <eriol@mornie.org>  Tue, 27 Jul 2010 04:41:14 +0200
