Source: pyth
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Daniele Tricoli <eriol@mornie.org>
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 poppler-utils,
 python-all (>=2.6.6-3),
 python-bs4,
 python-docutils,
 python-nose,
 python-reportlab,
 python-setuptools
Standards-Version: 4.1.3
Homepage: https://github.com/brendonh/pyth
Vcs-Git: https://salsa.debian.org/python-team/packages/pyth.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyth
Testsuite: autopkgtest-pkg-python

Package: python-pyth
Architecture: all
Depends:
 ${python:Depends},
 ${misc:Depends},
 python-bs4,
 python-docutils,
 python-reportlab
Description: Python text markup and conversion
 Pyth is a library used to manipulate different formats of marked-up text.
 .
 The following list of document formats are currently available:
  * XHTML (fully supported: read, write)
  * RTF (fully supported: read, write)
  * PDF (only output)
 .
 It also can generate documents from Python markup a la Nevow's stan and has
 limited experimental support for LaTeX.
